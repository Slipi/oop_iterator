//Iterator
interface mainIterator { 
    getNext() : music;
    hasMore() : boolean;
} 
//IteratableCollection
interface musicApp {
    createSequenceIterator(profile: Profile) : iterator ;
    createMixIterator(profile: Profile)      : iterator ;
}
//ConcreteIterator
class iterator implements mainIterator{
    private type : string;
    private mApp : musicApp;
    private profile: Profile;

    private currentPosition : number = -1;
    private Playlist : music[] = [];
    
    constructor(app : musicApp, profile: Profile, type: string){
        this.mApp = app;
        this.profile = profile;
        this.type = type;
    }

    private TimesPlayed:number = 0; //для mix'a
    getNext(){
        if(this.type == "sequence"){
            if (this.hasMore()){
                this.currentPosition++;
                this.Playlist[this.currentPosition].print();
                return this.Playlist[this.currentPosition];
            }
        }
        else{
            if (this.hasMore()){
                this.TimesPlayed++;
                this.currentPosition = this.transposition();
                this.Playlist[this.currentPosition].print();
                return this.Playlist[this.currentPosition];
            }
        }
        return this.Playlist[this.currentPosition];
    }
    private InitPlaylist(){
            this.Playlist = this.profile.Playlist;
    }
    hasMore(){
        this.InitPlaylist();
        if(this.type == "sequence"){
            return this.currentPosition < this.Playlist.length - 1;
        }
        else{
            return this.TimesPlayed < this.Playlist.length;
        }
    }

    private tmpArray : number[] = [];
    transposition() : number{
        let i = 0;
        let flag : boolean = true;
        while(flag){
          i = Math.floor(Math.random()*this.Playlist.length)
          if(this.tmpArray.indexOf(i) == -1){
              this.tmpArray.push(i);
              flag = false;
          }
        }
        return i;
    }
}

class ListeningMusic{
    iterator : iterator;
    constructor(iterator:iterator){
        this.iterator = iterator;
    }
    listening(){
        while(this.iterator.hasMore()){
            this.iterator.getNext();
        }
    }
}
//ConcreteCollection
class YandexMusic implements musicApp{
    profiles : Profile[] = [];
    constructor(profile: Profile){
        this.profiles.push(profile);
    }
    createSequenceIterator(profile: Profile) : iterator{
        return new iterator(this, profile, "sequence");
    }
    createMixIterator(profile: Profile) : iterator{
        return new iterator(this, profile, "mix");
    }
}


class Application{
    app : musicApp;
    profile: Profile;
    constructor(app : musicApp, profile:Profile){
        this.app = app;
        this.profile = profile;
    }
    Listening(type:string){
        if(type == "sequence"){
            let play : ListeningMusic =  new ListeningMusic(this.app.createSequenceIterator(this.profile));
            play.listening();
        }
        if(type == "mix"){
            let play : ListeningMusic =  new ListeningMusic(this.app.createMixIterator(this.profile));
            play.listening();
        }
    }
}

class Profile{
    profileId : string = "";
    Playlist : music[] = [];
    constructor(profileId:string){
        this.profileId = profileId;
    }
    addMusic(song : music){
        this.Playlist.push(song);
    }
}

class music{
   artist   : string;
   songName : string;
   constructor(artist:string, song:string){
       this.artist = artist;
       this.songName = song;
   }
   print(){
       console.log(this.artist, this.songName);
   }
}

console.clear();
let profile : Profile = new Profile("ZXC");
profile.addMusic(new music("Muse", ""));
profile.addMusic(new music("LinkinPark", ""));
profile.addMusic(new music("Skillet", ""));
profile.addMusic(new music("Nickelback", ""));
profile.addMusic(new music("Hollywood Undead", ""));
profile.addMusic(new music("Three Days Grace", ""));
let app : YandexMusic = new YandexMusic(profile);
let application : Application = new Application(app, profile);
application.Listening("mix");